PICOS Sphinx Theme
==================

PICOS Sphinx Theme (``picos-sphinx-theme``) is a `Sphinx
<https://www.sphinx-doc.org/>`_ theme forked from the `Faculty Sphinx Theme
<https://github.com/facultyai/faculty-sphinx-theme>`_ which in turn is based on
the `Read the Docs Sphinx Theme <https://sphinx-rtd-theme.readthedocs.io/>`_.

You can see the theme in action on the `PICOS Documentation
<https://picos-api.gitlab.io/picos/>`_.

Installation
------------

.. code-block:: bash

    pip install picos-sphinx-theme

Usage
-----

Load the ``"picos_sphinx_theme"`` extension and set ``html_theme`` to
``"picos"``. Additionally, you can set the following options via
``html_theme_options``:

- ``"gitlab_ribbon"``: Whether to add the *Fork me on GitLab* ribbon.
- ``"gitlab_ribbon_url"``: GitLab repository URL.
